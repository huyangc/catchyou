﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;

namespace CatchYou
{
    class GoogleS
    {
       
        public string KeyWord { get; set; }
        public int PageNum { get; set; }
        public int ProcessNum { get; set; }
        public ManualResetEvent mre { get; set; }
        public string path { get; set; }
        public Process pro { get; set; }
        public int flag_searchtype { get; set; }
        public string CookieFilePath { get; set; }
        public GoogleS(Process pro,int flag_searchtype, string KeyWord, int PageNum, int ProcessNum,string path,string CookieFilePath)
        {
            this.CookieFilePath = CookieFilePath;
            this.KeyWord = KeyWord;
            this.PageNum = PageNum;
            this.ProcessNum = ProcessNum;
            this.path = path;
            this.pro = pro;
            this.flag_searchtype = flag_searchtype;
        }
        
    }
}
