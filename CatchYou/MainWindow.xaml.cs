﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SQLite;
using Microsoft.WindowsAPICodePack.Dialogs;
using Microsoft.Win32;
/*
 * 代码中的变量解释：
 * B_开头代表这个变量是一个按钮变量
 * TB_开头表示这个变量是一个TextBox变量
 * LV_开头表示这个变量是一个ListView变量
 * 全局变量均为首字母大写，局部变量均为小写
 * 
 * @hzf
 * 
 */
namespace CatchYou
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        #region 变量
        private int TotalPage = 30;                //表示总共需要搜索的页数
        private string KeyWord;                    //表示搜索关键词
        private int ProcessNum = 10;               //表示同时进行搜索的进程数
        private string WordBookPath = "";          //表示单词本的路径
        private string DBtable = "urlData";        //表示导出数据库的表名
        private int SearchedCount = 0;             //表示已经搜索过的数量
        private const string Port_Patten = ":(\\d{1,4}|[1-5]\\d{4}|6[0-4]\\d{3}|65[0-4]\\d{2}|655[0-2]\\d{1}|6553[0-5])";
        private const string IP_Patten = @"(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])";
        private HashSet<Data> HashS = new HashSet<Data>();
        private Thread StartSearch;
        Thread StartWordBookSearch;
        private string ServerType = "";
        private bool UseWordBook = false;
        #endregion

        public MainWindow()
        {
            InitializeComponent();
        }

        //开始搜索按钮所对应的点击操作：
        #region 搜索按钮点击操作
        private void B_Search_Click(object sender, RoutedEventArgs e)
        {
            HashS.Clear();
            this.LV_SearchResult.Items.Clear();
            this.TB_TotalResultCount.Text = "0";
            SearchedCount =0;
            if (UseWordBook == false)
            {
                if (this.TB_KeyWord.Text == "")
                    MessageBox.Show("请输入关键词！");
                else
                {
                    KeyWord = this.TB_KeyWord.Text.Replace(" ", "+");

                    this.B_Search.IsEnabled = false;
                    this.B_Cancel.IsEnabled = true;
                    this.B_Export.IsEnabled = false;
                    StartSearch = new Thread(new ThreadStart(init_Search));
                    StartSearch.Start();
                }
            }
            else
            {
                if (WordBookPath == "")
                    MessageBox.Show("请先选择单词本路径！");
                else
                {
                    this.B_Search.IsEnabled = false;
                    this.B_Cancel.IsEnabled = true;
                    this.B_Export.IsEnabled = false;
                    StartSearch = new Thread(new ParameterizedThreadStart(init_wordbook_Search));
                    StartSearch.Start(WordBookPath);
                }
            }
        }
        #endregion

        //初始化以单词本中的关键词为搜索关键词的相应搜索
        #region 初始化单词本中的关键词作为搜索关键词操作
        private void init_wordbook_Search(object obj)
        {
            string WordBookPath = (string)obj;
            StreamReader strReader = new StreamReader(WordBookPath);
            string sLine = "";
            while (sLine != null)
            {
                sLine = strReader.ReadLine();
                if (sLine != null)
                {
                    SearchedCount = 0;
                    KeyWord = sLine;
                    StartWordBookSearch = new Thread(new ThreadStart(init_Search));
                    StartWordBookSearch.Start();
                    StartWordBookSearch.Join();
                }
                Thread.Sleep(5*60 * 1000);
            }
            this.B_Search.Dispatcher.Invoke(
                   new Action(
                       delegate
                       {
                           B_Search.IsEnabled = true;
                       }
               ));
            this.B_Export.Dispatcher.Invoke(
                new Action(
                    delegate
                    {
                        B_Export.IsEnabled = true;
                    }
            ));
            this.TB_TotalResultCount.Dispatcher.Invoke(
                new Action(
                    delegate
                    {
                        TB_TotalResultCount.Text = this.LV_SearchResult.Items.Count.ToString();
                    }
            ));
            
        }
        #endregion

        //初始化搜索函数，通过线程池来条用具体的搜索函数进行相应搜索
        #region 初始化搜索函数，通过线程池来条用具体的搜索函数进行相应搜索
        private void init_Search()
        {
            List<ManualResetEvent> manualresetevent = new List<ManualResetEvent>();
            ThreadPool.SetMaxThreads(ProcessNum, ProcessNum);
            for (; SearchedCount < TotalPage; SearchedCount++)
            {
                GoogleS gs = new GoogleS(new Process(),0,KeyWord,SearchedCount,SearchedCount,"ProxyFunction_GoogleS.exe",SearchedCount.ToString());
                gs.mre = new ManualResetEvent(false);
                manualresetevent.Add(gs.mre);
                ThreadPool.QueueUserWorkItem(exec_Search,gs);
                Thread.Sleep(3000);
            }
            ManualResetEvent.WaitAll(manualresetevent.ToArray(),60*1000);
            manualresetevent.Clear();
            if (WordBookPath == "")
            {
                this.B_Search.Dispatcher.Invoke(
                    new Action(
                        delegate
                        {
                            B_Search.IsEnabled = true;
                        }
                ));
                this.B_Export.Dispatcher.Invoke(
                    new Action(
                        delegate
                        {
                            B_Export.IsEnabled = true;
                        }
                ));
                this.TB_TotalResultCount.Dispatcher.Invoke(
                    new Action(
                        delegate
                        {
                            TB_TotalResultCount.Text = this.LV_SearchResult.Items.Count.ToString();
                        }
                ));
            }
        }
        #endregion

        //在每次进行搜索前先获得Google的Cookie然后使用这个Cookie去进行搜索
        #region 获取Google Cookie
        private void GetGoogleCookie(string path,string cookiefilepath)
        {
            Process pro = new Process();
            pro.StartInfo.FileName = path;                                     //设定程序执行路径
            pro.StartInfo.Arguments = cookiefilepath;    //设定执行参数  
            pro.StartInfo.UseShellExecute = false;                                //关闭Shell的使用  
                                    
             
            pro.StartInfo.CreateNoWindow = true;                                  //设置不显示窗口  
            pro.Start();                                                          //进程启动 

            pro.WaitForExit(10*1000);                                               //限定进程所运行的时间不超过30s
           
        }
        #endregion

        //真正执行Search操作，并将Google搜索得到的URL进行进一步操作，得到相应服务器的类型。
        #region 执行搜索
        private void exec_Search(object obj)
        {

            GoogleS gs = obj as GoogleS;
            GetGoogleCookie("ProxyuFunction_GoogleCookie.exe", gs.CookieFilePath);
            gs.pro.StartInfo.FileName = gs.path;                                     //设定程序执行路径
            gs.pro.StartInfo.Arguments = gs.flag_searchtype + " " + gs.KeyWord + " " + gs.PageNum + " " + gs.ProcessNum+" "+gs.CookieFilePath;    //设定执行参数  
            gs.pro.StartInfo.UseShellExecute = false;                                //关闭Shell的使用  
            gs.pro.StartInfo.RedirectStandardInput = true;                           //重定向标准输入
            gs.pro.StartInfo.RedirectStandardOutput = true;                          //重定向标准输出  
            gs.pro.StartInfo.RedirectStandardError = true;                           //重定向错误输出  
            gs.pro.StartInfo.CreateNoWindow = true;                                  //设置不显示窗口  
            try
            {
                gs.pro.Start();                                                          //进程启动 
            }
            catch (Exception)
            {
                return;
            }
            gs.pro.WaitForExit(30000);                                               //限定进程所运行的时间不超过30s
            if (!gs.pro.HasExited)
                gs.pro.Kill();

            string s = gs.pro.StandardOutput.ReadToEnd();                           //从输出流获得结果
            if (s.Trim() != "couldn't download html files" && s != "")
            {
                if (s.Trim() == "need to use proxy") { return; }
                    
                string[] sArray = Regex.Split(s, "\r\n", RegexOptions.IgnoreCase);
                foreach (string i in sArray)
                {
                    if (i != "")
                    {
                        Data dt = new Data();
                        int end_str;
                        if (ServerType != "")
                            dt.server_type = ServerType;
                        Match m = Regex.Match(i, Port_Patten);
                        if (m.Success)
                        {
                            string port = m.ToString().Substring(1);
                            dt.port = port;
                            end_str = i.IndexOf(':', 6);
                            if (end_str != -1)
                                dt.url = i.Substring(0, end_str);
                            else
                                dt.url = i;

                        }
                        else
                        {
                            end_str = i.IndexOf('/', 8);
                            if (end_str == -1)
                                dt.url = i;
                            else
                                dt.url = i.Substring(0, end_str);
                            dt.port = "80";
                        }
                        string t_ip = GetIpByUrl(dt.url);
                        if (t_ip != "false")
                            dt.ip = t_ip;
                        try
                        {
                            string strl;//存储编码

                            WebRequest wb = WebRequest.Create(dt.url);//请求资源
                            wb.Timeout = 5000;
                            WebResponse webRed = wb.GetResponse();//响应请求
                            string contenttype = webRed.ContentType;
                            string encode = "utf-8";
                            int begin_num = contenttype.IndexOf("charset=");
                            if(begin_num != -1)
                                encode = contenttype.Substring(begin_num + 8);
                            Stream redweb;
                            StreamReader sr;
                            StringBuilder sb;
                            try
                            {
                                var oencoding = Encoding.GetEncoding(encode);
                                redweb = webRed.GetResponseStream();//返回数据存入流中
                                sr = new StreamReader(redweb, oencoding);//从流中读出数据
                                sb = new StringBuilder();//可变字符
                            }
                            catch (Exception)
                            {
                                redweb = webRed.GetResponseStream();//返回数据存入流中
                                sr = new StreamReader(redweb, Encoding.UTF8);//从流中读出数据
                                sb = new StringBuilder();//可变字符
                            }
                                
                            while ((strl = sr.ReadLine()) != null)
                            {

                                sb.Append(strl);//读出数据存入可变字符中
                                if (strl.IndexOf("</title>") != -1)
                                    break;
                            }
                            string d = @"<title>(?<title>[^<]*)</title>";
                            string middle = Regex.Match(sb.ToString(), d).ToString();

                            string re = middle.Substring(middle.IndexOf("<title>") + 7, middle.IndexOf("</title>") - 7 - middle.IndexOf("<title>"));
                            dt.page_title = re.Trim();

                        }
                        catch (Exception)
                        {
                        }
                        dt.server_type = KeyWord;
                        AddData(dt);
                       
                    }
                }
            }
            FileInfo file = new FileInfo(gs.ProcessNum + ".xml");
            if (file.Exists)
            {
                try
                {
                    file.Delete();                                                      //删除单个文件
                }
                catch (Exception) { }
            }
            FileInfo CookieFile = new FileInfo(gs.CookieFilePath);
            if (CookieFile.Exists)
            {
                try
                {
                    CookieFile.Delete();
                }
                catch (Exception) { }
            }
            gs.mre.Set();
        }
        #endregion

        //由URL获取远程主机的ip地址
        #region 根据URL获取远程主机IP
        private string GetIpByUrl(string url)
        {
            if (url.IndexOf("https://") != -1)
                url = url.Substring(8);
            else if (url.IndexOf("http://") != -1)
                url = url.Substring(7);
            if (Regex.Match(url, IP_Patten).Success)
                return url;
            try
            {
                IPHostEntry ipEntry = Dns.GetHostEntry(url);
                IPAddress[] ipAddr = ipEntry.AddressList;
                string[] strAddr = new string[ipAddr.Length];
                return ipAddr[0].ToString();
            }
            catch (Exception)
            {
                return "false";
            }

        }
        #endregion

        //将数据加入到ListView中
        #region 将数据加入到ListView
        private void AddData(Data dt)
        {
            this.LV_SearchResult.Dispatcher.Invoke(
                new Action(
                    delegate
                    {
                        LV_SearchResult.Items.Add(dt);
                    }));
        }
        #endregion

        //Setting界面中的保存按钮所对应的点击操作
        #region 保存按钮点击
        private void B_SaveConfig_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                TotalPage = int.Parse(this.TB_SearchPage.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("请不要逗我，请输入数字！");
                this.TB_SearchPage.Text = "";
            }
            if (TotalPage > 50)
            {
                MessageBox.Show("页数应该小于50");
                TotalPage = 50;
            }
            try
            {
                ProcessNum = int.Parse(this.TB_ProcessNum.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("请不要逗我，请输入数字！");
                this.TB_ProcessNum.Text = "";
            }
            if (TotalPage < ProcessNum)
            {
                MessageBox.Show("你确定你的输入是正确的么？");
                ProcessNum = TotalPage;
            }
            ServerType = TB_ServerType.Text;
        }
        #endregion

        //对应于右键点击之后的清除重复项操作
        #region 清除重复项
        private void ClearRepeatedItem(object sender, RoutedEventArgs e)
        {
            if (this.B_Search.IsEnabled == false)
                MessageBox.Show("当前程序还未运行完成");
            else
            {
                for (int i = 0; i < LV_SearchResult.Items.Count; i++)
                {
                    if (!HashS.Add((Data)LV_SearchResult.Items[i]))
                    {
                        LV_SearchResult.Items.RemoveAt(i);
                        i--;
                    }
                }
                HashS.Clear();
                this.TB_TotalResultCount.Text = LV_SearchResult.Items.Count.ToString();
            }
        }
        #endregion

        //对应于取消按钮点击操作
        #region 取消按钮操作
        private void B_Cancel_Click(object sender, RoutedEventArgs e)
        {
            Thread CancelThread = new Thread(new ThreadStart(exec_CanCel_Click));
            CancelThread.Start();
        }
        void exec_CanCel_Click(){
            int temp_SearchedCount = SearchedCount;
             SearchedCount = TotalPage;
            if (StartSearch != null)
            {
                if (StartSearch.IsAlive)
                    StartSearch.Abort();
            }
            
            Thread.Sleep(7000);
            Process[][] AliveProcess = {System.Diagnostics.Process.GetProcessesByName("ProxyFunction_GoogleS"),System.Diagnostics.Process.GetProcessesByName("ProxyuFunction_GoogleCookie")};
            for(int i = 0;i<AliveProcess.Length;i++){
                for (int j = 0; j < AliveProcess[i].Length; j++)
                {
                    AliveProcess[i][j].Kill();
                }
            }
            this.B_Search.Dispatcher.Invoke(
                new Action(
                    delegate
                    {
                        B_Search.IsEnabled = true;
                    }
            ));
            this.B_Export.Dispatcher.Invoke(
                new Action(
                    delegate
                    {
                        B_Export.IsEnabled = true;
                    }));
            this.B_Cancel.Dispatcher.Invoke(
                new Action(
                    delegate
                    {
                        B_Cancel.IsEnabled = false;
                    }
            ));
            this.TB_TotalResultCount.Dispatcher.Invoke(
                new Action(
                    delegate
                    {
                        TB_TotalResultCount.Text = LV_SearchResult.Items.Count.ToString();
                    }
            ));
            for (int i = 0; i < temp_SearchedCount; i++)
            {
                FileInfo file = new FileInfo(i + ".xml");
                FileInfo file1 = new FileInfo(i.ToString());
                try
                {
                    if (file.Exists)
                        file.Delete();
                    if (file1.Exists)
                        file1.Delete();
                }
                catch (Exception) { }
            }
        }
        #endregion

        //对应于相应的导出到数据库的操作
        #region 导出到数据库
        private void B_Export_Click(object sender, RoutedEventArgs e)
        {
            /*var dlg = new CommonOpenFileDialog();
            dlg.IsFolderPicker = true;
            dlg.EnsurePathExists = true;
            dlg.Multiselect = false;
            dlg.DefaultDirectory = "";
            string path = "";
            if (dlg.ShowDialog() == CommonFileDialogResult.Ok)
            {
                if (dlg.FileName.LastIndexOf("\\") != dlg.FileName.Length - 1)
                    path = dlg.FileName + "\\" + DBname;
                else
                    path = dlg.FileName + DBname;
                this.TB_ExportPath.Text = path;
                Thread ExportAsSQLite = new Thread(new ParameterizedThreadStart(exec_Export));
                ExportAsSQLite.Start(path);
            }
            else
            {
                Thread ExportAsSQLite = new Thread(new ParameterizedThreadStart(exec_Export));
                ExportAsSQLite.Start(DBname);
            }*/
            SaveFileDialog sfd = new SaveFileDialog();
            string path = "";
            sfd.Filter = "SQLite Database files (*.sqlite)|*.sqlite|All files (*.*)|*.*";
            sfd.FilterIndex = 2;
            sfd.RestoreDirectory = true;
            sfd.OverwritePrompt = false;
            sfd.DefaultExt = "sqlite";
            if (true == (bool)sfd.ShowDialog())
            {
                path = sfd.FileName;
                this.TB_ExportPath.Text = path;
                Thread ExportAsSQLite = new Thread(new ParameterizedThreadStart(exec_Export));
                ExportAsSQLite.Start(path);
            }
        }

        private void exec_Export(object obj)
        {
            string dbPath = (string)obj;
            FileInfo dbFile = new FileInfo(dbPath);
            if (!dbFile.Exists)
                SQLiteConnection.CreateFile(dbPath);
            SQLiteConnection dbConn = new SQLiteConnection("Data Source=" + dbPath);
            dbConn.Open();
            SQLiteCommand dbCmd = dbConn.CreateCommand();
            try
            {
                dbCmd.CommandText = "create table "+DBtable+"(URL varchar(100),IP varchar(20),Port varchar(5),_Type varchar(20),Server_Type varchar(50),Page_Title varchar(100),OS varchar(20),primary key(URL))";
                dbCmd.ExecuteNonQuery();
            }
            catch (Exception) { }
            SQLiteTransaction tran = dbConn.BeginTransaction();
            dbCmd.Transaction = tran;
            for (int i = 0; i < this.LV_SearchResult.Items.Count; i++)
            {
                Data dt = LV_SearchResult.Items[i] as Data;
                try
                {
                    dbCmd.CommandText = "insert into " + DBtable + " values(@URL,@IP,@Port,@Type,@Server_Type,@Page_Title,@OS)";
                    dbCmd.Parameters.AddWithValue("@URL", dt.url);
                    dbCmd.Parameters.AddWithValue("@IP", dt.ip);
                    dbCmd.Parameters.AddWithValue("@Port", dt.port);
                    dbCmd.Parameters.AddWithValue("@Type", dt.type);
                    dbCmd.Parameters.AddWithValue("@Server_Type", dt.server_type);
                    dbCmd.Parameters.AddWithValue("@Page_Title", dt.page_title);
                    dbCmd.Parameters.AddWithValue("@OS", dt.os);
                    dbCmd.ExecuteNonQuery();
                }
                catch (SQLiteException) { }
            }
            tran.Commit();
            MessageBox.Show("导出完成！");
        }
        #endregion

        //对应于选择单词本
        #region 选择单词本
        private void B_WordBookBrowser_Click(object sender, RoutedEventArgs e)
        {
            var dlg = new CommonOpenFileDialog();
            dlg.IsFolderPicker = false;
            dlg.EnsurePathExists = true;
            dlg.Multiselect = false;
            dlg.DefaultDirectory = "";
            
            if (dlg.ShowDialog() == CommonFileDialogResult.Ok)
            {
                TB_WordBookPath.Text = dlg.FileName;
                WordBookPath = dlg.FileName;
            }
        }
        private void CB_UseWordBook_Click(object sender, RoutedEventArgs e)
        {
             if (CB_UseWordBook.IsChecked == true && B_WordBookBrowser.IsEnabled == false)
            {
                B_WordBookBrowser.IsEnabled = true;
                UseWordBook = true;
            }
            else if (CB_UseWordBook.IsChecked == false && B_WordBookBrowser.IsEnabled == true)
            {
                B_WordBookBrowser.IsEnabled = false;
                UseWordBook = false;
            }
        }
        #endregion
    }
}