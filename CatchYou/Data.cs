﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatchYou
{
    class Data
    {
        public string url { get; set; }
        public string ip { set; get; }
        public string port { get; set; }
        public string type { get; set; }
        public string server_type { get; set; }
        public string page_title { get; set; }
        public string os { get; set; }
        public Data(string curl = "Unknown", string cip = "Unknown", string cport = "Unknown", string ctype = "Web Server", string cserver_type = "Unknown", string cpage_title = "Unknown", string cos = "Unknown")
        {
            url = curl;
            ip = cip;
            port = cport;
            type = ctype;
            server_type = cserver_type;
            page_title = cpage_title;
            os = cos;
        }
        public override string ToString()
        {
            return url + ip + port + type + server_type + page_title + os;
        }
        public override bool Equals(object obj)
        {
            
            Data dt = obj as Data;
            if (dt == null)
                return false;
            if (this.url == dt.url)
                return true;
            else
                return false;
        }
        public override int GetHashCode()
        {
            return 0;
        }
    }
}
